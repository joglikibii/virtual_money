<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $table = 'transactions';

    protected $fillable = ['initiator', 'ref', 'sender', 'recipient', 'amount', 'type','description','type_of'];
}