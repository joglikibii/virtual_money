<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUser extends Mailable
{
    use Queueable, SerializesModels;
public $name;
public $recipient;
public $themessage;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $recipient, $themessage)
    {
        //
        $this->name = $name;
        $this->recipient = $recipient;
        $this->themessage = $themessage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.transactions')
            ->subject('New Transaction on Pikash')
            ->from('no-reply@pikash.net', 'Pikash Money Transfer');
    }
}
