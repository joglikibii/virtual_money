<?php

namespace App\Http\Controllers\Core;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\AfricasTalkingGateway;
use Illuminate\Http\Exception;
use App\Mail\NotifyUser;
use Illuminate\Support\Facades\Mail;

class MessagingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public static function sendMessage($name, $recipient, $email, $password){
        $username   = "joglikipp";
        $apikey     = "dbdb3f151c2aebe3aed01d80dfb6722aefb9f66d4ea38ecb475f4900b61fa20e";
        // Specify the numbers that you want to send to in a comma-separated list
        // Please ensure you include the country code (+254 for Kenya in this case)
        $recipients = $recipient;
        // And of course we want our recipients to know what we really do
        $message    = "Hi ".$name.", Welcome to PiKash Money Transfer. We have credited your account with KES. 100,000. Logon using E-Mail: ".$email." and Password: ".$password.". ^PiKash Team.";
        // Create a new instance of our awesome gateway class
        $gateway    = new AfricasTalkingGateway($username, $apikey);
        /*************************************************************************************
          NOTE: If connecting to the sandbox:
          1. Use "sandbox" as the username
          2. Use the apiKey generated from your sandbox application
             https://account.africastalking.com/apps/sandbox/settings/key
          3. Add the "sandbox" flag to the constructor
          $gateway  = new AfricasTalkingGateway($username, $apiKey, "sandbox");
        **************************************************************************************/
        // Any gateway error will be captured by our custom Exception class below, 
        // so wrap the call in a try-catch block
        try 
        { 
          // Thats it, hit send and we'll take care of the rest. 
          $results = $gateway->sendMessage($recipients, $message);
                    
          foreach($results as $result) {
            // status is either "Success" or "error message"
            echo " Number: " .$result->number;
            echo " Status: " .$result->status;
            echo " MessageId: " .$result->messageId;
            echo " Cost: "   .$result->cost."\n";
          }
        }
        catch ( AfricasTalkingGatewayException $e )
        {
          echo "Encountered an error while sending: ".$e->getMessage();
        }
     }

          public static function sendPassMessage($name, $recipient, $email, $password){
        $username   = "joglikipp";
        $apikey     = "dbdb3f151c2aebe3aed01d80dfb6722aefb9f66d4ea38ecb475f4900b61fa20e";
        // Specify the numbers that you want to send to in a comma-separated list
        // Please ensure you include the country code (+254 for Kenya in this case)
        $recipients = $recipient;
        // And of course we want our recipients to know what we really do
        $message    = "Hi ".$name.", Your Password was Successfully Changed. Your New Password: ".$password." . ^PiKash";
        // Create a new instance of our awesome gateway class
        $gateway    = new AfricasTalkingGateway($username, $apikey);
        /*************************************************************************************
          NOTE: If connecting to the sandbox:
          1. Use "sandbox" as the username
          2. Use the apiKey generated from your sandbox application
             https://account.africastalking.com/apps/sandbox/settings/key
          3. Add the "sandbox" flag to the constructor
          $gateway  = new AfricasTalkingGateway($username, $apiKey, "sandbox");
        **************************************************************************************/
        // Any gateway error will be captured by our custom Exception class below, 
        // so wrap the call in a try-catch block
        try 
        { 
          // Thats it, hit send and we'll take care of the rest. 
          $results = $gateway->sendMessage($recipients, $message);
                    
          foreach($results as $result) {
            // status is either "Success" or "error message"
            echo " Number: " .$result->number;
            echo " Status: " .$result->status;
            echo " MessageId: " .$result->messageId;
            echo " Cost: "   .$result->cost."\n";
          }
        }
        catch ( AfricasTalkingGatewayException $e )
        {
          echo "Encountered an error while sending: ".$e->getMessage();
        }
     }
    public static function sendNotification($recipient, $message){
        $username   = "joglikipp";
        $apikey     = "dbdb3f151c2aebe3aed01d80dfb6722aefb9f66d4ea38ecb475f4900b61fa20e";
        // Specify the numbers that you want to send to in a comma-separated list
        // Please ensure you include the country code (+254 for Kenya in this case)
        $recipients = $recipient;
        // And of course we want our recipients to know what we really do
        $message    = $message;
        // Create a new instance of our awesome gateway class
        $gateway    = new AfricasTalkingGateway($username, $apikey);
        /*************************************************************************************
        NOTE: If connecting to the sandbox:
        1. Use "sandbox" as the username
        2. Use the apiKey generated from your sandbox application
        https://account.africastalking.com/apps/sandbox/settings/key
        3. Add the "sandbox" flag to the constructor
        $gateway  = new AfricasTalkingGateway($username, $apiKey, "sandbox");
         **************************************************************************************/
        // Any gateway error will be captured by our custom Exception class below,
        // so wrap the call in a try-catch block
        try
        {
            // Thats it, hit send and we'll take care of the rest.
            $results = $gateway->sendMessage($recipients, $message);

            foreach($results as $result) {
                // status is either "Success" or "error message"
                echo " Number: " .$result->number;
                echo " Status: " .$result->status;
                echo " MessageId: " .$result->messageId;
                echo " Cost: "   .$result->cost."\n";
            }
        }
        catch ( AfricasTalkingGatewayException $e )
        {
            echo "Encountered an error while sending: ".$e->getMessage();
        }
    }

    public static function sendAirtime($recipient, $amount){
        //Specify your credentials
        $username   = "joglikipp";
        $apiKey     = "dbdb3f151c2aebe3aed01d80dfb6722aefb9f66d4ea38ecb475f4900b61fa20e";
//Specify the phone number/s and amount in the format shown
//Example shown assumes we want to send KES 100 to two numbers
// Please ensure you include the country code for phone numbers (+254 for Kenya in this case)
// Please ensure you include the country code for phone numbers (KES for Kenya in this case)

        $user = [];
        $user['phoneNumber'] = $recipient;
        $user['amount'] = 'KES '.$amount;
        $user;
        $recipients = array(
            array("phoneNumber"=>$user['phoneNumber'], "amount"=>$user['amount'])
        );
        //35.56-30.76
//Convert the recipient array into a string. The json string produced will have the format:
// [{"amount":"KES 100", "phoneNumber":"+254711XXXYYY"},{"amount":"KES 100", "phoneNumber":"+254733YYYZZZ"}]
//A json string with the shown format may be created directly and skip the above steps
        $recipientStringFormat = json_encode($recipients);
//Create an instance of our awesome gateway class and pass your credentials
        $gateway = new AfricasTalkingGateway($username, $apiKey);
        /*************************************************************************************
        NOTE: If connecting to the sandbox:
        1. Use "sandbox" as the username
        2. Use the apiKey generated from your sandbox application
        https://account.africastalking.com/apps/sandbox/settings/key
        3. Add the "sandbox" flag to the constructor
        $gateway  = new AfricasTalkingGateway($username, $apiKey, "sandbox");
         **************************************************************************************/
// Thats it, hit send and we'll take care of the rest. Any errors will
// be captured in the Exception class as shown below
        try {
            $results = $gateway->sendAirtime($recipientStringFormat);
            foreach($results as $result) {
                echo $result->status;
                echo $result->amount;
                echo $result->phoneNumber;
                echo $result->discount;
                echo $result->requestId;

                //Error message is important when the status is not Success
                echo $result->errorMessage;
            }
        }
        catch(AfricasTalkingGatewayException $e){
            echo $e->getMessage();
        }
    }

    public static function sendEmail($name, $recipient, $themessage){

      Mail::to($recipient)->send(new NotifyUser($name, $recipient, $themessage));

    }
}
