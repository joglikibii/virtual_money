<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::where('sender', Auth::user()->id)->OrWhere('recipient', Auth::user()->id)->orderBy('id','desc')->get();
        $airtime = Transaction::where('sender', Auth::user()->id)->where('type_of', 'Airtime')->orderBy('id','desc')->get();

        return view('home')->with('transactions', $transactions)->with('airtime', $airtime);
    }
}
