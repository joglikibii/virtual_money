<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Core\MessagingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;
use SmoDav\Mpesa\Native\Mpesa;
use App\User;
use App\Transaction;
use Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
        $transactions = Transaction::where('sender', Auth::user()->id)->OrWhere('recipient', Auth::user()->id)->orderBy('id','desc')->get();
        return view('transactions')->with('transactions', $transactions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendMoney()
    {

        return view('send-money');
    }

    public function sendAirtime()
    {

        return view('send-airtime');
    }

    public function mpesaTopup()
    {

        return view('topup');
    }

    public function sendMoneyRequest(Request $request)
    {

        $recipient = User::where('mobile_no', $request->mobile_no)->first();

        $request['sender'] = Auth::user()->id;
        $request['recipient'] = $recipient->id;
        $request['initiator'] = Auth::user()->id;
        $request['type'] = 'Debit';
        $request['type_of'] = 'Cash';
        $request['ref'] = Self::generateId(10, $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');

        $recipient->account_balance = $recipient->account_balance + $request['amount'];
        $recipient->save();

        $sender = User::find(Auth::user()->id);
        $sender->account_balance = $sender->account_balance - $request['amount'];
        $sender->save();

        $save = Transaction::create($request->all());
        //return $save['created_at'];
        $date = date('d/m/Y', strtotime($save->created_at));
        $time = date('H:i A', strtotime($save->created_at));
        $amount = number_format($save->amount,2);

        $notifySender = MessagingController::sendNotification($sender->mobile_no,'' . $save->ref . ' Confirmed. Kes.' . $amount . ' sent to ' . $recipient->name . ' on ' . $date . ' at ' . $time . '. New PiKash Account Balance is Kes. '.number_format($sender->account_balance,2).'. ^PiKash.');
        $notifyRecipient = MessagingController::sendNotification($recipient->mobile_no, '' . $save->ref . ' Confirmed. You received Kes.' . $amount . ' from ' . $sender->name . ' on ' . $date . ' at ' . $time . '. New PiKash Account Balance is Kes. '.number_format($recipient->account_balance,2).'. ^PiKash.');
        $transactionnotifySender = MessagingController::sendEmail($sender->name, $sender->email,'' . $save->ref . ' Confirmed. Kes.' . $amount . ' sent to ' . $recipient->name . ' on ' . $date . ' at ' . $time . '. Your New PiKash Account Balance is Kes. '.number_format($sender->account_balance,2).'.');
        $transactionnotifyRecipient = MessagingController::sendEmail($recipient->name, $recipient->email, '' . $save->ref . ' Confirmed. You received Kes.' . $amount . ' from ' . $sender->name . ' on ' . $date . ' at ' . $time . '. Your New PiKash Account Balance is Kes. '.number_format($recipient->account_balance,2).'.');

        Alert::success('SUCCESS', 'You Sent KES. ' . $amount . ' to ' . $recipient->name . ' on ' . $date . ' at ' . $time . '. REF: ' . $save->ref . '')->autoClose(50000)->showCancelButton('OK');
        //return Session::pull('alert.config');
        return redirect()->back()->with('success', 'Transaction was successful. Sent KES. ' . $amount . ' to ' . $recipient->name . ' on ' . $date . ' at ' . $time . '. REF: ' . $save->ref . '');

    }

    public function sendAirtimeRequest(Request $request)
    {

        $recipient = User::where('mobile_no', $request->mobile_no)->first();

        $request['sender'] = Auth::user()->id;
        $request['recipient'] = $recipient->id;
        $request['initiator'] = Auth::user()->id;
        $request['type'] = 'Debit';
        $request['type_of'] = 'Airtime';
        $request['ref'] = Self::generateId(10, $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');

        //$recipient->account_balance = $recipient->account_balance + $request['amount'];
        //$recipient->save();
        $sendAirtime = MessagingController::sendAirtime($recipient->mobile_no, $request['amount']);

        $sender = User::find(Auth::user()->id);
        $sender->account_balance = $sender->account_balance - $request['amount'];
        $sender->save();

        $save = Transaction::create($request->all());
        //return $save['created_at'];
        $date = date('d/m/Y', strtotime($save->created_at));
        $time = date('H:i A', strtotime($save->created_at));
        $amount = number_format($save->amount,2);

        //$notifySender = MessagingController::sendNotification($sender->mobile_no,'' . $save->ref . ' Confirmed. Kes.' . $amount . ' sent to ' . $recipient->name . ' on ' . $date . ' at ' . $time . '. New PiKash Account Balance is Kes. '.number_format($sender->account_balance,2).'. ^PiKash.');
        //$notifyRecipient = MessagingController::sendNotification($recipient->mobile_no, '' . $save->ref . ' Confirmed. You received Kes.' . $amount . ' from ' . $sender->name . ' on ' . $date . ' at ' . $time . '. New PiKash Account Balance is Kes. '.number_format($recipient->account_balance,2).'. ^PiKash.');

        Alert::success('SUCCESS', 'You Sent Airtime worth KES. ' . $amount . ' to ' . $recipient->name . '['.$recipient->mobile_no.'] on ' . $date . ' at ' . $time . '. REF: ' . $save->ref . '')->autoClose(50000)->showCancelButton('OK');
        //return Session::pull('alert.config');
        return redirect()->back()->with('success', 'Transaction was successful. Sent KES. ' . $amount . ' worth of Airtime to ' . $recipient->name . ' on ' . $date . ' at ' . $time . '. REF: ' . $save->ref . '');

    }

    public static function generateId($length, $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    {
        $str = '';
        $count = strlen($charset);
        while ($length--) {
            $str .= $charset[mt_rand(0, $count - 1)];
        }

        return 'PK' . $str;
    }
    public static function getType($id)
    {
        $transaction = Transaction::find($id);
        if($transaction->recipient == Auth::user()->id){
            if($transaction->type_of == 'Airtime'){
                return 'DEBIT - '.$transaction->type_of.'';
                }else{
                    return 'CREDIT - '.$transaction->type_of.'';
                }
            }else{
            return 'DEBIT - '.$transaction->type_of.'';
        }
    }

    public function sendMpesaRequest(Request $request)
    {

        $response = mpesa()->request($request->amount)->from($request->phone)->usingReferenceId('your PiKash Account')->transact();

        $transactionId = $response->transactionId;

        session()->put('transactionId', $transactionId);

        Alert::success('SUCCESS', 'We have sent a request to your Mobile Phone. Kindly complete the transaction and CLICK the COMPLTE BUTTON upon receiving a confirmation message from M-PESA.')->autoClose(50000)->showCancelButton('OK');
        return redirect()->back()->with('success', 'We have sent a request to your Mobile Phone. Kindly complete the transaction and CLICK the COMPLETE BUTTON upon receiving a confirmation message from M-PESA.');
    }

    public function validateMpesaPayment($transactionId)
    {
        //return $status = mpesa()->validate($transactionId);
        try {
            $status = mpesa()->validate($transactionId);
            //return $status = json_encode($status);
            if ($status->response->success == true) {

                // $payment = MpesaPayments::where('transaction_reference', $status->response->transaction_number)->get();
                // if(count($payment) >= 1){
                //     return back()->withSuccess('We have already credited your account with your M-PESA Payment. Transaction ID: '.$status->response->transaction_number.'');
                // }
                $request = [];
                $request['sender'] = 1;
                $request['recipient'] = Auth::user()->id;
                $request['initiator'] = Auth::user()->id;
                $request['type'] = 'Credit';
                $request['type_of'] = 'Cash';
                $request['ref'] = Self::generateId(10, $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
                $request['amount'] = $status->response->amount;
                $request['description'] = 'M-PESA Top-Up Transaction '.$status->response->transaction_number.' of Kes. '.number_format($status->response->amount,2).' received from '.$status->response->number.' on '.$status->response->transaction_date.'.';
                $save = Transaction::create($request);

                $recipient = User::find(Auth::user()->id);
                $recipient->account_balance = $recipient->account_balance + $request['amount'];
                $recipient->save();

                $newtop = intval($request['amount']);

                //session()->forget('transactionId');
                $date = date('d/m/Y', strtotime($save->created_at));
                $time = date('H:i A', strtotime($save->created_at));

                Alert::success('SUCCESS', 'Top successful. Your account has been credited with Ksh.' . number_format($newtop,2) . '. New PiKash balance is Ksh' . number_format($recipient->account_balance, 2) . '. M-PESA Transaction ID: ' . $status->response->transaction_number . '.')->autoClose(50000)->showCancelButton('OK');

                $notifyRecipient = MessagingController::sendNotification($recipient->mobile_no, '' . $save->ref . ' Confirmed. You received Kes.' . $request['amount'] . ' from M-PESA Top-up on ' . $date . ' at ' . $time . '. New PiKash Account Balance is Kes. '.number_format($recipient->account_balance,2).'. M-PESA Reference Id: '.$status->response->transaction_number.'. ^PiKash.');

                session()->forget('transactionId');
                return back()->withSuccess('Your Top-up was successful. Your account has been credited with Ksh.' . number_format($newtop,2) . '. New PiKash balance is Ksh' . number_format($recipient->account_balance, 2) . '. M-PESA Transaction ID: ' . $status->response->transaction_number . '.');

            }

        } catch (\Exception $ex) {
            return $ex;
            return back()->withError($ex.'Sorry we had a technical error while trying to process your payment. Kindly click the Complete button upon receiving an M-Pesa Message');
        }

        return redirect()->back()->with('success', 'We have sent a request to your Mobile Phone. Kindly complete the transaction and CLICK the COMPLTE BUTTON upon receiving a confirmation message from M-PESA.');
    }

    public function checkUser(Request $request)
    {
        $input = $request->all();
        if ($request->ajax()) {
            if (DB::table('users')->where('mobile_no', '=', $request->input('phone'))->exists()) {
                return response()->json(['success' => true, 'created' => false, 'msg' => 'is available.']);
            } else {
                return response()->json(['success' => true, 'created' => true, 'msg' => 'not PiKash user.']);
            }
        }
    }

}
