<?php

namespace App\Http\Controllers;

use App\Http\Requests\AjaxUserExistsRequest;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

        public function profile()
    {
        $user = User::find(auth()->user()->id);
        return view('profile')->with('user', $user);
    }


    public function settings()
    {
        $user = User::find(auth()->user()->id);
        return view('settings')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //return $request->all();
        $user = User::find($id);
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->mobile_no = $request['mobile_no'];
        $user->bio = $request['bio'];
        $user->save();

        if($user){
            return redirect()->back()->with('success', 'Profile Updated successfully');
        }else{
            return redirect()->back()->with('error','There was an error updating your profile');
        }
    }

        public function update_settings(Request $request, $id)
    {
        //
        //return $request->all();
        $user = User::find($id);
        

        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not match with the password you provided. Please try again.");
        }
 
        if(strcmp($request->get('current_password'), $request->password) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

 
        //Change Password
        $user = User::find($id);
        $user->password = bcrypt($request->password);
        $user->save();

        \App\Http\Controllers\Core\MessagingController::sendPassMessage($user['name'], $user['mobile_no'], $user['email'], $request['password']);
 
        return redirect()->back()->with("success","Password changed successfully !");
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function getName($id){
        $user = User::find($id);
        if($user == false){
            if($id == '1'){
                return 'M-PESA TOPUP';
            }elseif($id == '0') {
                return 'PiKash';
            }
        }else {
                return $user->name;
        }
    }

    public function user_exists(AjaxUserExistsRequest $request){
        $user = User::where('mobile_no', $request['mobile_no'])->first();
        if(!empty($user)){
            if($user->id == Auth::user()->id){
                return 'ownAccount';
            }else{
                return $user->name;
            }
        }
    }
}
