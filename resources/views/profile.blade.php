@extends('layouts.app')
@section('content')
@include('layouts.top_nav')
    <!-- End Top Navigation -->

    <!-- Left navbar-header -->
@include('layouts.left_nav')
    <!-- Left navbar-header end -->
    <!-- Page Content -->
    <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">My Profile</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- .row -->
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ session()->get('success') }} </div>
                @endif
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <form class="form-horizontal form-material" action="{{url('/profile',$user->id)}}" method="post">
                      {{csrf_field()}}
                  <div class="form-group">
                    <label class="col-md-12">Full Name</label>
                    <div class="col-md-12">
                      <input type="text" placeholder="{{$user->name}}" value="{{$user->name}}" name="name" class="form-control form-control-line">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="example-email" class="col-md-12">Email</label>
                    <div class="col-md-12">
                      <input type="email" placeholder="{{$user->email}}" value="{{$user->email}}" class="form-control form-control-line" name="email" id="example-email" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Mobile Number</label>
                    <div class="col-md-12">
                      <input type="text" value="{{$user->mobile_no}}" class="form-control form-control-line" name="mobile_no" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Bio</label>
                    <div class="col-md-12">
                      <textarea rows="5" class="form-control form-control-line" name="bio">{{$user->bio}}</textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <button class="btn btn-success">Update Profile</button>
                    </div>
                  </div>
                </form>
        </div>
      </div>
    </div>
@endsection
