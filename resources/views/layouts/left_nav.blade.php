    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse slimscrollsidebar">
            <ul class="nav" id="side-menu">
            </br>
                <li> <a href="{{url('/home')}}" class="waves-effect"><i class="ti-wallet" data-icon="lg"></i> <span class="hide-menu"> &nbsp; My Account </span></a></li>
                <li> <a href="{{url('/sendMoney')}}" class="waves-effect"><i class="ti-share" data-icon="v"></i> <span class="hide-menu"> &nbsp; Send Money </span></a></li>
                <li> <a href="{{url('/top-up')}}" class="waves-effect"><i class="ti-upload" data-icon="v"></i> <span class="hide-menu"> &nbsp; Top up Account </span></a></li>
                <li> <a href="{{url('/home')}}" class="waves-effect"><i class="ti-download" data-icon="v"></i> <span class="hide-menu"> &nbsp; Withdraw Money </span></a></li>
                <li> <a href="{{url('/sendAirtime')}}" class="waves-effect"><i class="ti-receipt" data-icon="v"></i> <span class="hide-menu"> &nbsp; Buy Airtime </span></a></li>
                <li> <a href="{{url('/transactions')}}" class="waves-effect"><i class="ti-list" data-icon="v"></i> <span class="hide-menu"> &nbsp; My Transactions </span></a></li>
        </ul>
        </div>
    </div>