<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>PiKash Money Transfer</title>
    <!-- Bootstrap Core CSS -->
    <script src="{{url('plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <link href="{{url('bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{url('css/animate.css')}}" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="{{url('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css')}}" rel="stylesheet">
    
    <!--Owl carousel CSS -->
    <link href="{{url('plugins/bower_components/owl.carousel/owl.carousel.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('plugins/bower_components/owl.carousel/owl.theme.default.css')}}" rel="stylesheet" type="text/css" />
    <!-- animation CSS -->
    <link href="{{url('css/animate.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{url('css/style.css')}}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{url('css/colors/gray-dark.css')}}" id="theme"  rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src=".{{url('plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>
  <link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="{{url('plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- start - This is for export functionality only -->
    <script src="{{url('plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>



</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>

</div>
<div id="wrapper">
  @yield('content')
<!-- /.container-fluid -->
    <footer class="footer text-center"> 2018 &copy; PiKash Money Transfer | All Rights Reserved</footer>
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<!-- Bootstrap Core JavaScript -->
<script src="{{url('bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Menu Plugin JavaScript -->
<script src="{{url('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')}}"></script>

<!--slimscroll JavaScript -->
<script src="{{url('js/jquery.slimscroll.js')}}"></script>
<!--Wave Effects -->
<script src="{{url('js/waves.js')}}"></script>
<!--weather icon -->
<script src="{{url('plugins/bower_components/skycons/skycons.js')}}"></script>
<!--Morris JavaScript -->
<script src="{{url('plugins/bower_components/raphael/raphael-min.js')}}"></script>
<!-- jQuery for carousel -->
<script src="{{url('plugins/bower_components/owl.carousel/owl.carousel.min.js')}}"></script>
<script src="{{url('plugins/bower_components/owl.carousel/owl.custom.js')}}"></script>
<!-- Sparkline chart JavaScript -->
<script src="{{url('plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{url('plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js')}}"></script>
<!--Counter js -->
<script src="{{url('plugins/bower_components/waypoints/lib/jquery.waypoints.js')}}"></script>
<script src="{{url('plugins/bower_components/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{url('plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js')}}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{url('js/custom.min.js')}}"></script>
<script src="{{url('js/widget.js')}}"></script>
<!--Style Switcher -->
<script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

</body>
<script src="{{url('plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}"></script>
@include('sweetalert::alert')

<script>
    $(document).ready(function () {
        $('#transactions').DataTable();
        $(document).ready(function () {
            var table = $('#example').DataTable({
                "columnDefs": [
                    {
                        "visible": false
                        , "targets": 2
                    }
                ]
                , "order": [[2, 'asc']]
                , "displayLength": 25
                , "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#transactions tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[5];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                }
                else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#transactions').DataTable({
        dom: 'Bfrtip'
        , buttons: [
            'pageLength',
            { extend: 'copyHtml5', footer: false },
            { extend: 'excelHtml5', footer: false },
            { extend: 'csvHtml5', footer: false },
            { extend: 'pdfHtml5', footer: false }
        ]
    });
    $('#card').DataTable({
        dom: 'Bfrtip'
        , buttons: [
            'pageLength',
            { extend: 'copyHtml5', footer: false },
            { extend: 'excelHtml5', footer: false },
            { extend: 'csvHtml5', footer: false },
            { extend: 'pdfHtml5', footer: false }
        ]
    });
    $(document).ready(function() {
        $('#card').DataTable( {
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\KES. ,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 4, { search: 'applied'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                // Update footer
                $( api.column( 4 ).footer() ).html(
                    'KES'+ total +''
                );
            }
        } );
    } );
</script>
</html>
