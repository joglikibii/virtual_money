<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header"><a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)"
                                  data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
        <div class="top-left-part hidden-xs">
            <a class="logo" href="{{url('/')}}"><b>
                    <img src="{{url('plugins/images/eliteadmin-logo.png')}}" style="height:70px" alt="home"
                         class="dark-logo"/>
                </b><span class="hidden-xs"><
            <img src="{{('plugins/images/eliteadmin-text-dark.png')}}" alt="home" class="light-logo"/></span></a>
        </div>
        <ul class="nav navbar-top-links navbar-left hidden-xs">
            <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i
                            class="icon-arrow-left-circle ti-menu"></i></a></li>
            <li>
                <form role="search" class="app-search hidden-xs">
                    <input type="text" placeholder="Search..." class="form-control">
                    <a href=""><i class="fa fa-search"></i></a>
                </form>
            </li>
        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">
            <!-- /.Megamenu -->
            <li class="mega-dropdown">
                <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><span
                            class="hidden-xs">{{auth()->user()->name}}</span> <i class="ti-user"></i></a>
                <ul class="dropdown-menu animated flipInY">
                    <li><a href="{{url('/profile')}}"><i class="ti-user"></i> My Profile</a></li>
                    <li><a href="{{url('/home')}}"><i class="ti-wallet"></i> My Balance
                            KES. {{number_format(auth()->user()->account_balance,2)}}</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{url('/settings')}}"><i class="ti-settings"></i> Account Setting</a></li>
                    <li role="separator" class="divider"></li>
                    <form class="" action="{{url('logout')}}" method="post">
                        {{ csrf_field() }}
                        <li>
                            <button class="btn btn-success btn-block" type="submit" name="logout">Logout</button>
                        </li>
                    </form>
                </ul>
                <!-- /.dropdown -->
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
</nav>
<!-- End Top Navigation -->