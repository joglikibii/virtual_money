@extends('layouts.app')
@section('content')
    @include('layouts.top_nav')
    <!-- End Top Navigation -->

    <!-- Left navbar-header -->
    @include('layouts.left_nav')
    <!-- Left navbar-header end -->
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-6">
                        <div class="white-box">
                            <h2 class="box-title m-b-0">Top Up My Account</h2>
                            <div class="row">
                                @if(session()->has('success'))
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                            &times;
                                        </button>{{session('success')}}
                                    </div>
                                @endif
                                @if(session()->has('error'))
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                            &times;
                                        </button>{{session('error')}}
                                    </div>
                                @endif
                                <div class="col-sm-12 col-xs-12">
                                    <form action="{{url('topup/request/mpesa')}}" id="sendRequest" method="post">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label for="exampleInputuname">M-PESA Number</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-mobile"></i></div>
                                                <input name="phone" type="text" class="form-control"
                                                       id="exampleInputuname" placeholder="Phone Number"
                                                       value="{{Auth::user()->mobile_no}}" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Amount</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-money"></i></div>
                                                <input name="amount" type="number" class="form-control"
                                                       id="exampleInputEmail1" min='10' placeholder="Enter Amount"
                                                       required>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                                        <button class="btn btn-success waves-effect waves-light m-r-10" id="sendMoney">
                                            <i class="fa fa-send"></i> Send
                                        </button>
                                    </form>

                                </div>
                            </div>
                        </div>
                        @if(session()->has('transactionId'))
                            <div class="col-sm-12 col-xs-12">
                                <button onclick="location.href = '{{url('/validate/transaction',session('transactionId'))}}'"
                                        class="btn btn-success btn-block">Click to Complete
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
@endsection
