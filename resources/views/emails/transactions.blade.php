<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

  <link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
  <div class="container">
  <div class="row">
            
            <div class="wrapper">
                <div class="logo_header">
                    <a href="pikash.net"><img src="http://demo.pikash.net/plugins/images/eliteadmin-logo.png" height="100px"/></a>
                </div>
                <div class="email_banner">
                    
                </div>
                <div class="email_body">
                    <h2 class="text-center">New successful Transaction on PiKash</h2>
                    <p class="text-center">
                      Hello {{$name}},
                    </p>
                    <p>
                      {{$themessage}}
                    </p>
                    <p class="text-center">
                      Regards,
                    </p>
                    <p class="text-center">
                      Pikash Team.
                    </p>
                   
                </div>
            </div>
            
  </div>
</div>
<style type="text/css">
  .wrapper{width:60%; margin:5% auto;height:100vh;  box-shadow:0 0 2px #aaa; font-family:Hind;}
.logo_header{width:100%; height:70px;background:gray; padding:10px;}
.email_body{width:100%; padding:0 15px;}
.receipt_list{width:100%;}
.receipt_list .left_list{float:left; width:60%;}
.receipt_list .right_list{float:left; width:40%;}
 .left_list b,.right_list b{width:100%; float:left; margin:0 0 10px 0;}
 .left_list span,.right_list span{width:100%; float:left; margin:0 0 5px 0;}
 .right_list span{text-align:left; padding-left:15%;}
 .list_divider{width:100%; border-top:1px solid rgba(0,0,0,0.2);float:left;}
 .invoice_trans{width:100%;float:left; margin:5px 0;}
.invoice_left{float:left; width:60%;}
.invoice_right{float:left; width:40%;}

</style>