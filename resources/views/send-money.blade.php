@extends('layouts.app')
@section('content')
@include('layouts.top_nav')
    <!-- End Top Navigation -->

    <!-- Left navbar-header -->
@include('layouts.left_nav')
    <!-- Left navbar-header end -->
    <!-- Page Content -->
    <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-6">
                        <div class="white-box">
                            <h2 class="box-title m-b-0">Send Money</h2>
                            <div class="row">
                              @if(session()->has('success'))
                              <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>{{session('success')}}
                              </div>
                              @endif
                              @if(session()->has('error'))
                              <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>{{session('error')}}
                              </div>
                              @endif
                              @if (session('success'))
                                <script>
                                    $( document ).ready(
                                        swal("{{ session('success') }}")
                                    });
                                </script>
                            @endif
                                <div class="col-sm-12 col-xs-12">
                                    <form action="{{url('sendMoney')}}" id="sendMoney" method="post">
                                      {{csrf_field()}}
                                        <div class="form-group">
                                            <label for="exampleInputuname">Recipient's Number</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-mobile"></i></div>
                                                <input name="mobile_no" type="number" class="form-control" id="mobile_no" value="254" placeholder="Phone Number" required>
                                              </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Amount</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-money"></i></div>
                                                <input name="amount" type="number" class="form-control" id="exampleInputEmail1" min='10' placeholder="Enter Amount" required>
                                              </div>
                                        </div>
                                        <a href="#" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                                        <button class="btn btn-success waves-effect waves-light m-r-10" id="sendMoney"><i class="fa fa-send"></i> Send</button>
                                    </form>
                                    
                                </div>
                            </div>
                        </div>
                    </div>       
        </div>
      </div>
    </div>
        <script type="text/javascript">
            $('#mobile_no').change(function(e){
                //alert('alert');
                e.preventDefault();

                var mobile_no = $('#mobile_no').val(),
                    $this = this; //aliased so we can use in ajax success function

                $.ajax({
                    type: 'POST',
                    url: '/user_exists',
                    data: {mobile_no: mobile_no},
                    success: function(data){
                        if(data == ''){
                            swal({
                                title: "Oops",
                                text: "Your recipient does not have a PiKash Account",
                                type: "warning",
                                buttons: [
                                    'Go Back'
                                ],
                                dangerMode: true,
                            }).then(function(isConfirm) {
                                if (isConfirm) {
                                    //input.value="";
                                    document.getElementById("mobile_no").value = '';
                                }
                            });
                        }else {
                            if (data == 'ownAccount') {
                                swal({
                                    title: "Ha!",
                                    text: "You are trying to send money to your own account",
                                    type: "warning",
                                    buttons: [
                                        'Go Back'
                                    ],
                                    dangerMode: true,
                                }).then(function(isConfirm) {
                                    if (isConfirm) {
                                        //input.value="";
                                        document.getElementById("mobile_no").value = '';
                                    }
                                });
                            } else {
                                swal({
                                    title: "Awesome!",
                                    text: "We found your recipient " + data + " ",
                                    type: "success",
                                    buttons: [
                                        'Go Back'
                                    ],
                                    dangerMode: true,
                                })
                            }
                        }
                    }

                });
            });
        </script>
@endsection
