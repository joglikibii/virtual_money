@extends('layouts.app')

@section('content')
    <section id="wrapper" class="login-register">
        <img src="../plugins/images/eliteadmin-logo.png" style="height:70px;" align="center" alt="home" class="dark-logo" />
        <div class="login-box">
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ session()->get('success') }} </div>
                @endif
            <div class="white-box">
                <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('register') }}">
                        @csrf
                    <h3 class="box-title m-b-20">Sign Up</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" type="text"  value="{{ old('name') }}" required autofocus placeholder="Name" name="name">
                        </div>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input id="email"  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" type="email" required placeholder="E-Mail">
                        </div>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input id="mobile_no" class="form-control{{ $errors->has('mobile_no') ? ' is-invalid' : '' }}" type="number" name="mobile_no" value="{{ old('mobile_no') }}" required placeholder="+254">
                        </div>
                        @if ($errors->has('mobile_no'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mobile_no') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Sign Up</button>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>Already have an account? <a href="{{url('/login')}}" class="text-primary m-l-5"><b>Sign In</b></a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
