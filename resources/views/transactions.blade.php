@extends('layouts.app')
@section('content')
    @include('layouts.top_nav')
    <!-- Left navbar-header -->
    @include('layouts.left_nav')
    <!-- Left navbar-header end -->
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">My Transactions</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="white-box">
                        <h2 class="box-title m-b-0">My Recent Transactions</h2>
                        <div class="table-responsive">
                            <table id="transactions" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Sender</th>
                                    <th>Recipient</th>
                                    <th>Transaction ID</th>
                                    <th>Amount</th>
                                    <th>Type</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Sender</th>
                                    <th>Recipient</th>
                                    <th>Transaction ID</th>
                                    <th>Amount</th>
                                    <th>Type</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php $count = 0;?>
                                @foreach($transactions as $transaction)
                                    <tr>
                                        <?php $count = $count + 1; ?>
                                        <td>{{$count}}</td>
                                        <td>{{\App\Http\Controllers\UserController::getName($transaction->sender)}}</td>
                                        <td>{{\App\Http\Controllers\UserController::getName($transaction->recipient)}}</td>
                                        <td>{{$transaction->ref}}</td>
                                        <td>KES. {{number_format($transaction->amount,2)}}</td>
                                        <td>
                                            @if(App\Http\Controllers\TransactionController::getType($transaction->id) == 'CREDIT - Cash')
                      <button class="btn btn-success btn-sm">CREDIT - Cash</button>
                          @else
                          <button class="btn btn-danger btn-sm text-caps">DEBIT - {{$transaction->type_of}}</button>
                      @endif
                                        </td>
                                        <td>{{date('dS M Y h:i A', strtotime($transaction->created_at))}}</td>
                                        <td>
                                            <button class="btn btn-success btn-sm">SUCCESS</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
@endsection
