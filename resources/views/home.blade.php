@extends('layouts.app')
@section('content')
@include('layouts.top_nav')
    <!-- Left navbar-header -->
@include('layouts.left_nav')
    <!-- Left navbar-header end -->
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">My Dashboard</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- .row -->
            <div class="row">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="white-box">
                                <h3 class="box-title">My Account</h3>
                                <ul class="list-inline two-part">
                                    <li><i class="ti-wallet text-success"></i></li>
                                    <li class="text-right"><span class="counter">KES {{number_format(auth()->user()->account_balance,0)}}</span></li>
                                <a href="{{url('/top-up')}}" class="btn btn-success">Top Up</a>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="white-box">
                                <h3 class="box-title">My Groups[dummy]</h3>
                                <ul class="list-inline two-part">
                                    <li><i class="icon-people text-info"></i></li>
                                    <li class="text-right"><span class="counter">2</span></li>
                                    <button class="btn btn-success">View All</button>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="white-box">
                                <h3 class="box-title">Transactions</h3>
                                <ul class="list-inline two-part">
                                    <li><i class="icon-list text-purple"></i></li>
                                    <li class="text-right"><span class="counter">{{count($transactions)}}</span></li>
                                    <a href="{{url('transactions')}}" class="btn btn-success">View All</a>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="white-box">
                                <h3 class="box-title">Airtime Purchase History</h3>
                                <ul class="list-inline two-part">
                                    <li><i class="ti-receipt text-danger"></i></li>
                                    <li class="text-right"><span class="counter">{{count($airtime)}}</span></li>
                                    <button class="btn btn-success">View All</button>
                                </ul>
                            </div>
                        </div>
                    </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="white-box">
                        <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h2 class="box-title m-b-0">My Recent Transactions</h2>
            <div class="table-responsive">
            <table id="transactions" class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Sender</th>
                  <th>Recipient</th>
                  <th>Transaction ID</th>
                  <th>Amount</th>
                  <th>Type</th>
                  <th>Date</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              <?php $count = 0 ;?>
              @foreach($transactions as $transaction)
                <tr>
                    <?php $count = $count+1; ?>
                  <td>{{$count}}</td>
                  <td>{{\App\Http\Controllers\UserController::getName($transaction->sender)}}</td>
                  <td>{{\App\Http\Controllers\UserController::getName($transaction->recipient)}}</td>
                  <td>{{$transaction->ref}}</td>
                  <td>KES. {{number_format($transaction->amount, 2)}}</td>
                  <td>
                      @if(App\Http\Controllers\TransactionController::getType($transaction->id) == 'CREDIT - Cash')
                      <button class="btn btn-success btn-sm">CREDIT - Cash</button>
                          @else
                          <button class="btn btn-danger btn-sm">DEBIT - {{$transaction->type_of}}</button>
                      @endif
                  </td>
                  <td>{{date('dS M Y h:i A', strtotime($transaction->created_at))}}</td>
                  <td><button class="btn btn-success btn-sm">SUCCESS</button></td>
                </tr>
              @endforeach
              </tbody>
            </table>
            <a href="{{url('transactions')}}" class="btn btn-success">View All</a>
            </div>
          </div>
        </div>
                    </div>
                </div>
            </div>
            <!--row -->
        </div>
@endsection
