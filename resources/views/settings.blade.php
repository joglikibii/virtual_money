@extends('layouts.app')
@section('content')
@include('layouts.top_nav')
    <!-- End Top Navigation -->

    <!-- Left navbar-header -->
@include('layouts.left_nav')
    <!-- Left navbar-header end -->
    <!-- Page Content -->
    <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Change Password</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- .row -->
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ session()->get('success') }} </div>
                @endif
                @if(session()->has('error'))
                <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ session()->get('error') }} </div>
                @endif
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <form class="form-horizontal form-material" action="{{url('/settings',$user->id)}}" method="post">
                      {{csrf_field()}}
                  <div class="form-group">
                    <label class="col-md-12">Current Password *</label>
                    <div class="col-md-12">
                      <input type="password" name="current_password" class="form-control form-control-line" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="example-email" class="col-md-12">New Password *</label>
                    <div class="col-md-12">
                      <input type="password" class="form-control form-control-line" name="password" id="password" id="example-email" required >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Confirm Password *</label>
                    <div class="col-md-12">
                      <input type="password" class="form-control form-control-line" name="c_password"  id="c_password" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <button class="btn btn-success" onclick="return Validate()">Change Password</button>
                    </div>
                  </div>
                </form>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    function Validate() {
        var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("c_password").value;
        if (password != confirmPassword) {
            alert("Uh Oh! You Passwords do not match.");
            return false;
        }
        return true;
    }
</script>
@endsection
