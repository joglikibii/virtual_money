<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/profile', 'UserController@profile')->name('profile');
Route::get('/settings', 'UserController@settings')->name('settings');
Route::post('/profile/{id}', 'UserController@update')->name('update_profile');
Route::post('/settings/{id}', 'UserController@update_settings')->name('update_settings');

Route::post('/user_exists', 'UserController@user_exists');

//manage mpesa topups
Route::get('/transactions', 'TransactionController@index')->name('My Transactions');
Route::get('/top-up/', 'TransactionController@mpesaTopup')->name('Top Up Account');
Route::post('topup/request/mpesa', 'TransactionController@sendMpesaRequest');
Route::get('validate/transaction/{id}', 'TransactionController@validateMpesaPayment');

//manage virtual money transactions
Route::get('/sendMoney/', 'TransactionController@sendMoney')->name('Send Money');
Route::post('/sendMoney/', 'TransactionController@sendMoneyRequest')->name('Send Money');
Route::get('/sendAirtime/', 'TransactionController@sendAirtime')->name('Send Airtime');
Route::post('/sendAirtime/', 'TransactionController@sendAirtimeRequest')->name('Send Airtime');
Route::get('airtime', 'Core\MessagingController@sendAirtime');
Route::get('/tests', function(){

    $mpesa = new SmoDav\Mpesa\Native\Mpesa;
    
    $response = mpesa()->request(10)->from(254729553498)->usingReferenceId('your SMS Africa Account:15616115')->transact();

    return $response = json_encode($response);
});

Route::get('/validate/test/{id}', function($id){

    $mpesa = new SmoDav\Mpesa\Native\Mpesa;
    
            //return $transactionId;
       
            $status = mpesa()->validate($id);
            
                return $status = json_encode($status->response);
            
  //           sample response
  //    {
		// "desctiption": "The service request is processed successfully.",
		// "transaction_number": "MF80U7UUXU",
		// "transaction_date": "2018-06-08 15:12:05",
		// "transaction_id": "634a2595e4d621eb141207b9c03f49ad",
		// "number": "254729553498",
		// "amount": "10",
		// "success": true
		// }
    //Chcx1lqYbjyCfmo8X
    //http://localhost:8000/validate/transaction/Chcx1lqYbjyCfmo8X
            
        
});

    Route::get('currency', function() {
        $endpoint = 'convert';
        $access_key = '63a180bdd18be1b43edc130cc2932b44';
        $date = date('Y-m-d');
// initialize CURL:
        $ch = curl_init('http://apilayer.net/api/historical?access_key='.$access_key.'&date='.$date.'&format=1');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// get the (still encoded) JSON data:
        $json = curl_exec($ch);
        curl_close($ch);

// Decode JSON response:
        $conversionResult = json_decode($json, true);

// access the conversion result
        return $conversionResult['quotes']['USDKES'];
});

//Route::get('/test', 'Core\MessagingController@sendMessage');
